﻿Imports System.Drawing.Printing
Imports System.Net
Imports Newtonsoft.Json

Public Class Form1

    Dim server_url = "http://192.168.32.100/api/"

    Dim currentPrintingBookingCode As String = ""
    Dim currentPrintingInvoiceNumber As String = ""

    Dim maxLineLength = 32
    Dim StringToPrint As String = ""
    Dim LineLen As Integer
    Dim TextToPrint As String = ""

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'PrintDocument1.PrinterSettings.PrinterName = "EPSON TM-U220 Receipt"
        PrintDocument1.PrinterSettings.PrinterName = "EPSON TM-T82II Receipt5"
        'PrintDocument1.PrinterSettings.PrinterName = "Brother MFC-J200 Printer (Copy 1)"
    End Sub

    Public Sub PrintHeader()

        'send address name
        StringToPrint = "Ruko Elang Laut Blok B No. 5"
        LineLen = StringToPrint.Length
        Dim spcLen2 As New String(" "c, Math.Round((maxLineLength - LineLen) / 2))
        TextToPrint &= spcLen2 & StringToPrint & Environment.NewLine

        ' send city, state, zip
        StringToPrint = "Pantai Indah Kapuk, Jkt Utara"
        LineLen = StringToPrint.Length
        Dim spcLen3 As New String(" "c, Math.Round((maxLineLength - LineLen) / 2))
        TextToPrint &= spcLen3 & StringToPrint & Environment.NewLine

        ' send phone number
        StringToPrint = "(+62)21-29033081"
        LineLen = StringToPrint.Length
        Dim spcLen4 As New String(" "c, Math.Round((maxLineLength - LineLen) / 2))
        TextToPrint &= spcLen4 & StringToPrint & Environment.NewLine

        'send website
        StringToPrint = "www.flightdeckindonesia.com"
        LineLen = StringToPrint.Length
        Dim spcLen4b As New String(" "c, Math.Round((maxLineLength - LineLen) / 2))
        TextToPrint &= spcLen4b & StringToPrint & Environment.NewLine

        'send separator
        StringToPrint = "==============================================="
        LineLen = StringToPrint.Length
        TextToPrint &= StringToPrint & Environment.NewLine
    End Sub

    Public Sub ItemsToBePrinted(bookingCode As String, custName As String, instructor As String,
                                flightPkg As String, paymentType As String, dateTime As String,
                                price As Integer)

        currentPrintingBookingCode = bookingCode

        StringToPrint = "Booking Code   " & bookingCode
        LineLen = StringToPrint.Length
        Dim spcLen4b As New String(" "c, Math.Round((maxLineLength - LineLen) / 2))
        TextToPrint &= spcLen4b & StringToPrint & Environment.NewLine & Environment.NewLine
        LineLen = flightPkg.Length
        Dim spcLen1 As New String(" "c, Math.Round((maxLineLength - LineLen) / 2))
        TextToPrint &= spcLen1 & flightPkg & Environment.NewLine
        TextToPrint &= "===============================================" & Environment.NewLine & Environment.NewLine

        TextToPrint &= "Cust. Name : " & custName & Environment.NewLine
        TextToPrint &= "Instructor : " & instructor & Environment.NewLine
        TextToPrint &= "Date/Time  : " & dateTime & Environment.NewLine
        TextToPrint &= "---------------------------------------------" & Environment.NewLine
        TextToPrint &= "Price      : IDR " & price.ToString("N0") & Environment.NewLine
        TextToPrint &= "Payment    : " & paymentType & Environment.NewLine

    End Sub

    Public Sub SalesPrintOut(invoiceNumber As String, custName As String, paymentType As String, textItem As String)

        StringToPrint = "Invoice Number   " & invoiceNumber
        currentPrintingInvoiceNumber = invoiceNumber
        LineLen = StringToPrint.Length
        Dim spcLen4b As New String(" "c, Math.Round((maxLineLength - LineLen) / 2))
        TextToPrint &= spcLen4b & StringToPrint & Environment.NewLine & Environment.NewLine

        StringToPrint = "Customer : " & custName
        LineLen = StringToPrint.Length
        Dim spcLen1 As New String(" "c, Math.Round((maxLineLength - LineLen) / 2))
        TextToPrint &= spcLen1 & StringToPrint & Environment.NewLine
        TextToPrint &= "===============================================" & Environment.NewLine & Environment.NewLine

        Dim total_price As Integer = 0
        Dim textItemArray As Array = textItem.Split(";")
        Console.WriteLine(textItemArray.Length)
        For i = 0 To textItemArray.Length - 1
            Dim line As String = textItemArray(i)
            If (line.Length > 0) Then
                Dim lineArray As Array = line.Split("|")
                Console.WriteLine(lineArray(1).ToString)
                Dim item As String = lineArray(1)
                Dim qty As Integer = Integer.Parse(lineArray(3))
                Dim price As Integer = Integer.Parse(lineArray(4))
                Dim price_subtotal As Integer = Integer.Parse(lineArray(4))
                Dim qtyxPrice As String = qty & " x " & price.ToString("N0")
                Dim subtotal As String = price_subtotal.ToString("N0")
                Dim separation As Integer = maxLineLength - qtyxPrice.Length - subtotal.Length

                TextToPrint &= item & Environment.NewLine
                TextToPrint &= qtyxPrice
                For j = 1 To separation
                    TextToPrint &= " "
                Next
                TextToPrint &= subtotal & Environment.NewLine

                total_price = total_price + price_subtotal
            End If
        Next

        TextToPrint &= "---------------------------------------------" & Environment.NewLine
        TextToPrint &= "Total Price : IDR " & total_price.ToString("N0") & Environment.NewLine
        TextToPrint &= "Payment     : " & paymentType & Environment.NewLine
    End Sub

    Dim receiptDataObj As JSON_Receipt
    Dim salesDataObj As JSON_Sales

    Private Sub GetBookingData()
        Dim uri As New Uri(server_url + "receipt")
        If (uri.Scheme = Uri.UriSchemeHttp) Then
            Dim request As HttpWebRequest = HttpWebRequest.Create(uri)
            request.Method = WebRequestMethods.Http.Get
            Dim response As HttpWebResponse = request.GetResponse()
            Dim reader As New IO.StreamReader(response.GetResponseStream())
            Dim tmp As String = reader.ReadToEnd()

            receiptDataObj = JsonConvert.DeserializeObject(Of JSON_Receipt)(tmp)
            ItemsToBePrinted(receiptDataObj.booking_code, receiptDataObj.customer_name,
                             receiptDataObj.instructor_name, receiptDataObj.package,
                             receiptDataObj.payment_method, receiptDataObj.date_time,
                             Integer.Parse(receiptDataObj.price))

            response.Close()
        End If
    End Sub
    Private Sub GetSalesData()
        Dim uri As New Uri(server_url + "sales_receipt")
        If (uri.Scheme = Uri.UriSchemeHttp) Then
            Dim request As HttpWebRequest = HttpWebRequest.Create(uri)
            request.Method = WebRequestMethods.Http.Get
            Dim response As HttpWebResponse = request.GetResponse()
            Dim reader As New IO.StreamReader(response.GetResponseStream())
            Dim tmp As String = reader.ReadToEnd()
            
            salesDataObj = JsonConvert.DeserializeObject(Of JSON_Sales)(tmp)
            SalesPrintOut(salesDataObj.invoice_number.ToString,
                          salesDataObj.customer_name,
                          salesDataObj.payment_method,
                          salesDataObj.items_text)


            response.Close()
        End If
    End Sub

    Private Function DeletePrintJob() As Boolean
        Dim uri As New Uri(server_url + "receipt/delete/" + currentPrintingBookingCode)
        If (uri.Scheme = Uri.UriSchemeHttp) Then
            Dim request As HttpWebRequest = HttpWebRequest.Create(uri)
            request.Method = WebRequestMethods.Http.Get
            Dim response As HttpWebResponse = request.GetResponse()
            Dim reader As New IO.StreamReader(response.GetResponseStream())
            Dim tmp As String = reader.ReadToEnd()

            response.Close()
            If tmp = "OK" Then
                currentPrintingBookingCode = ""
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    Private Function DeleteSalesPrintJob() As Boolean
        Dim uri As New Uri(server_url + "sales_receipt/delete/" + currentPrintingInvoiceNumber)
        If (uri.Scheme = Uri.UriSchemeHttp) Then
            Dim request As HttpWebRequest = HttpWebRequest.Create(uri)
            request.Method = WebRequestMethods.Http.Get
            Dim response As HttpWebResponse = request.GetResponse()
            Dim reader As New IO.StreamReader(response.GetResponseStream())
            Dim tmp As String = reader.ReadToEnd()

            response.Close()
            If tmp = "OK" Then
                currentPrintingInvoiceNumber = ""
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    Private Function CheckIfPrintJobExist() As Boolean
        Dim uri As New Uri(server_url + "receipt/check_exist")
        If (uri.Scheme = Uri.UriSchemeHttp) Then
            Dim request As HttpWebRequest = HttpWebRequest.Create(uri)
            request.Method = WebRequestMethods.Http.Get
            Dim response As HttpWebResponse = request.GetResponse()
            Dim reader As New IO.StreamReader(response.GetResponseStream())
            Dim tmp As String = reader.ReadToEnd()

            response.Close()
            If Integer.Parse(tmp) > 0 Then
                Console.WriteLine(tmp)
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function
    Private Function CheckIfSalesPrintJobExist() As Boolean
        Dim uri As New Uri(server_url + "sales_receipt/check_exist")
        If (uri.Scheme = Uri.UriSchemeHttp) Then
            Dim request As HttpWebRequest = HttpWebRequest.Create(uri)
            request.Method = WebRequestMethods.Http.Get
            Dim response As HttpWebResponse = request.GetResponse()
            Dim reader As New IO.StreamReader(response.GetResponseStream())
            Dim tmp As String = reader.ReadToEnd()

            response.Close()
            If Integer.Parse(tmp) > 0 Then
                Console.WriteLine(tmp)
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    Private Sub PrintDocument1_PrintPage(sender As Object, e As PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Static currentChar As Integer
        Dim textfont As Font = New Font("Courier New", 9, FontStyle.Regular)

        Dim h, w As Integer
        Dim left, top As Integer
        With PrintDocument1.DefaultPageSettings
            h = 0
            w = 0
            left = 0
            top = 0
        End With


        Dim lines As Integer = CInt(Math.Round(h / 1))
        Dim b As New Rectangle(left, top, w, h)
        Dim format As StringFormat
        format = New StringFormat(StringFormatFlags.LineLimit)
        Dim line, chars As Integer


        e.Graphics.MeasureString(Mid(TextToPrint, currentChar + 1), textfont, New SizeF(w, h), format, chars, line)
        e.Graphics.DrawString(TextToPrint.Substring(currentChar, chars), textfont, Brushes.Black, b, format)


        currentChar = currentChar + chars
        If currentChar < TextToPrint.Length Then
            e.HasMorePages = True
        Else
            e.HasMorePages = False
            currentChar = 0
        End If
    End Sub

    Private Sub ExecutePrintJob()
        TextToPrint = ""
        PrintHeader()
        GetBookingData()
        Dim printControl = New Printing.StandardPrintController
        PrintDocument1.PrintController = printControl
        Try
            PrintDocument1.Print()
            PrintDocument1.Print()
            DeletePrintJob()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub ExecuteSalesPrintJob()
        TextToPrint = ""
        PrintHeader()
        GetSalesData()
        Dim printControl = New Printing.StandardPrintController
        PrintDocument1.PrintController = printControl
        Try
            PrintDocument1.Print()
            DeleteSalesPrintJob()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        ExecutePrintJob()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Timer1.Enabled = True
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If CheckIfPrintJobExist() Then
            ExecutePrintJob()
        ElseIf CheckIfSalesPrintJobExist() Then
            ExecuteSalesPrintJob()
        End If
    End Sub
End Class


Public Class JSON_Receipt
    Public booking_code As String
    Public package As String
    Public customer_name As String
    Public instructor_name As String
    Public date_time As String
    Public price As String
    Public payment_method As String
End Class
Public Class JSON_Sales
    Public invoice_number As String
    Public customer_name As String
    Public payment_method As String
    Public items_text As String
End Class